//Simular el juego Craps
//Un jugador tira 2 dados y se suma los resultados
//1. Si en el primer tiro la suma es 7 o 11 el jugador gana
//2. Si en el primer tiro la suma es 2, 3 o 12 la casa gana
//3. Si la suma es 4, 5, 6, 8, 9, 10 en el primer tiro
//        El jugador sigue tirando hasta que salga la suma que le salio
//        si primero sale la suma 7, el jugador pierde.

package chapter06;

import java.util.Random;

public class Craps {
    // crea un generador de numeros aleatorios para usarlo en el metodo tirarDado
    private static final Random numerosAleatorios =  new Random();

    // enumeracion con constantes que representan el estado del juego
    private enum Estado {CONTINUA, GANO, PERDIO}

    // Constantes que representan tiros comunes de los dados
    private static final int DOS_UNOS = 2;
    private static final int TRES = 3;
    private static final int SIETE = 7;
    private static final int ONCE = 11;
    private static final int DOCE = 12;

    // Ejecuta un juego de craps
    public static void main(String[] args) {

        int miPunto = 0; // Punto si no gana o pierde en el primer tiro
        Estado estadoJuego; // puede contener CONTINUA, GANO O PERDIO

        int sumaDados = tirarDados(); // primer tiro de los dados

        // Determina el estado del juego y el punto con base en el primer tiro
        switch ( sumaDados ) {
            case SIETE: // gana con 7 en el primer tiro
            case ONCE: // gana con 11 en el primer tiro
                estadoJuego = Estado.GANO;
                break;
            case DOS_UNOS: // pierde con 2 en el primer tiro
            case TRES:     // pierde con 3 en el primer tiro
            case DOCE:     // pierde con 12 en el primer tiro
                estadoJuego = Estado.PERDIO;
                break;
            default:   // No guardo ni perdio por lo que guarda el punto
                estadoJuego = Estado.CONTINUA; // no ha terminado el juego
                miPunto = sumaDados; // guarda el punto
                System.out.printf("El punto es %d\n", miPunto);
                break;
        }

        // mientras el juego no este terminado
        while (estadoJuego == Estado.CONTINUA) { // No gano ni perdio
            sumaDados = tirarDados(); // tira los dados de nuevo

            // Determina el estado del jeugo
            if (sumaDados == miPunto) { // gana haciendo un punto
                estadoJuego = Estado.GANO;
            }else {
                if (sumaDados == SIETE) { // pierde al tirar el 7 antes del punto
                    estadoJuego = Estado.PERDIO;
                }
            }

            // muestra mensaje que gano o perdio
            if (estadoJuego == Estado.GANO) {
                System.out.println("El jugador gano");
            }else {
                if (estadoJuego == Estado.PERDIO) {
                System.out.println("El jugador perdio");
                }
            }
        }
    }

    // Tirar los dados, calcula la suma y muestra los resultados
    private static int tirarDados() {
        // Elige valores aleatorios para los 2 dados
        int dado1 = 1 + numerosAleatorios.nextInt(6);
        int dado2 = 1 + numerosAleatorios.nextInt(6);

        // suma los valores de los dados
        int suma = dado1 + dado2;

        System.out.printf("El jugador tiro %d + %d = %d\n", dado1, dado2, suma);
        return suma; // devuelve la suma de los dados
    }

}
