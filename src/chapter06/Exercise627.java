package chapter06;

import java.util.Scanner;

public class Exercise627 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter tow numbers");
        int a = sc.nextInt();
        int b = sc.nextInt();

        System.out.printf("The mcd for the two numbers are: %d\n",mcd(a,b));

    }

    private static int mcd(int a, int b) {
        int mcd = 1;
        if (a > b) {
            for (int i = 2; i <= b; i++) {
                if (a % i == 0 && b % i ==0) {
                    mcd = i;
                }
            }
        }else {
            for (int i = 2; i <= a; i++) {
                if (a % i == 0 && b % i ==0) {
                    mcd = i;
                }
            }
        }

        return mcd;

    }
}
