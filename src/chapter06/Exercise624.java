package chapter06;

public class Exercise624 {
    public static void main(String[] args) {

        for (int i = 1; i < 10000; i++) {
            if (esPerfecto(i)) {
                System.out.print(i + " : ");
                multiplos(i);
                System.out.println();
            }
        }

    }

    private static boolean esPerfecto(int numero) {

        int suma = 0;

        for (int i = 1; i < numero; i++) {
            if (esMultiplo(numero,i)) {
                suma += i;
            }
        }

//        if (numero == suma) {
//            return true;
//        } else {
//            return false;
//        }

        return numero == suma;
    }

    private static boolean esMultiplo(int numero, int factor) {
//        if (numero % factor == 0) {
//            return true;
//        } else {
//            return false;
//        }

        return numero % factor == 0;
    }

    private static void multiplos(int numero) {
        for (int i = 1; i < numero; i++) {
            if (esMultiplo(numero, i)) {
                System.out.printf("%d, ",i);
            }
        }
    }
}
