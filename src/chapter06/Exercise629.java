// Lanzar una moneda e indicar las veces que salio cara o cruz
package chapter06;

import java.util.Random;
import java.util.Scanner;

public class Exercise629 {
    enum ladosMoneda {CARA, CRUZ}
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean quiereLanzar;
        int opcionDeJuego;
        int cara = 0;
        int cruz = 0;
        do {
            System.out.println("Indique la opcion 1 o 2: ");
            System.out.println("1. Lanzar Moneda");
            System.out.println("2. Parar");
            opcionDeJuego = sc.nextInt();

            if (opcionDeJuego == 1) {
                tirar();
                if (tirar() == ladosMoneda.CARA) {
                    cara++;
                }
                if (tirar() == ladosMoneda.CRUZ){
                    cruz++;
                }
                quiereLanzar = true;
            }else {
                quiereLanzar = false;
            }
        }while (quiereLanzar);

        System.out.printf("Cara : %d veces\n", cara);
        System.out.printf("Cruz : %d veces\n", cruz);
    }

    private static ladosMoneda tirar() {
        Random aleatorio = new Random();
        int ladoCara = aleatorio.nextInt(2);

        if (ladoCara == 0) {
            return ladosMoneda.CRUZ;
        }else {
            return ladosMoneda.CARA;
        }
    }
}
