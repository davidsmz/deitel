package chapter06;

import java.util.Random;
import java.util.Scanner;

public class Exercise639 {
    private static int numeroPregunta = 1;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        Random numeroAleatorio = new Random();

        System.out.print("Indique el numero de alumnos: ");
        int nAlumnos = sc.nextInt();

        int respuesta; // respuesta que indica el alumno por operacion
        double rc = 0; // numero de respuestas correctas por alumno
        double ri = 0; // numero de respuestas incorrectas por alumno
        double rt; // numero de respuestas totales ( correctas e incorrectas)
        int contestaciones = 0; // numero de preguntas contestadas por el alumno

        // nivel de dificultad que es numero de digitos maximo
        // nivel 1 es para un digito
        // nivel 2 es para maximo 2 digitos
        // nivel 3 es para maximo 3 digitos, etc
        int dificultad;
        int operacion;
        for (int i = 1; i <= nAlumnos; i++) {

            System.out.printf("Alumno numero %d\n",i);
            System.out.print("Ingrese el nivel de dificultad: ");
            dificultad = sc.nextInt();
            System.out.println("Indique la operacion a utilizar con un numero: ");
            System.out.print("1. suma\n2. resta\n3. multiplicacion\n4. division\n5. mezcla\n");
            operacion = sc.nextInt();


            while (contestaciones < 10) {

                // creacion de los numeros aleatorios segun dificultad elegida
                int a = 1 + numeroAleatorio.nextInt(dificultad * 10 - 1);
                int b = 1 + numeroAleatorio.nextInt(dificultad * 10 - 1);


                do {
                    // operador aleatorio
                    int operacionAleatoria = 1 + numeroAleatorio.nextInt(4);
                    // mensaje aleatorio de respuesta correcta
                    int mrc = 1 + numeroAleatorio.nextInt(4);
                    // mensaje aleatorio de respuesta incorrecta
                    int mri = 1 + numeroAleatorio.nextInt(4);
                    int respuestaMomentaea;
                    if (operacion != 5) {
                        pregunta(a, b, operacion);
                        respuestaMomentaea = operador(a, b, operacion);
                    }else {
                        pregunta(a, b, operacionAleatoria);
                        respuestaMomentaea = operador(a, b, operacionAleatoria);
                    }

                    respuesta = sc.nextInt();
                    if (respuesta == respuestaMomentaea) {
                        respuestaCorrecta(mrc);
                        rc++;
                    }
                    if (respuesta != respuestaMomentaea){
                        respuestaIncorrecta(mri);
                        ri++;
                    }
                    contestaciones++;
                }while (contestaciones < 10); // fin do while

            } // fin while

            rt = rc + ri;
            if (rc / rt >= 0.75) {
                System.out.print("Felicidades\n");
            }
            if (ri / rt > 0.25){
                System.out.print("Por favor pida ayuda a su instructor\n");
            }

            rc = 0;
            ri = 0;
            contestaciones = 0;

        }

    }

    // metodo para realizar la pregunta
    private static void pregunta(int a, int b, int operacion) {
        if (operacion == 1) {
            System.out.printf("%d. Cuanto es %d + %d: \n",numeroPregunta, a, b);
            numeroPregunta++;
        }

        if (operacion == 2) {
            System.out.printf("%d. Cuanto es %d - %d: \n",numeroPregunta, a, b);
            numeroPregunta++;
        }

        if (operacion == 3) {
            System.out.printf("%d. Cuanto es %d * %d: \n",numeroPregunta, a, b);
            numeroPregunta++;
        }

        if (operacion == 4) {
            System.out.printf("%d. Cuanto es %d / %d: \n",numeroPregunta, a, b);
            numeroPregunta++;
        }

        if (operacion == 5) {
            pregunta(a, b, operacion);
        }
    }

    // Metodo para dar a elegir la operacion a utilizar
    private static int operador(int a, int b, int operacion) {

        if (operacion == 1) {
            return a + b;
        }
        if (operacion == 2) {
            return a - b;
        }

        if (operacion == 3) {
            return a * b;
        }
        if (operacion == 4) {
            return a / b;
        }

        return 0;
    }

    // Metodo que muestra los mensajes a la respuestas Correctas
    private static void respuestaCorrecta(int mrc) {
        switch (mrc) {
            case 1:
                System.out.println("Muy Bien!");
                break;
            case 2:
                System.out.println("Excelente!");
                break;
            case 3:
                System.out.println("Buen Trabajo!");
                break;
            case 4:
                System.out.println("Sigue así!");
                break;
        }
    }

    // Metodo que muestra los mensajes a la respuestas Incorrectas
    private static void respuestaIncorrecta(int mri) {
        switch (mri) {
            case 1:
                System.out.println("No. Por favor intentar de nuevo!");
                break;
            case 2:
                System.out.println("Incorrecto intenta una vez mas.");
                break;
            case 3:
                System.out.println("No te rindas!");
                break;
            case 4:
                System.out.println("No. Sigue intentando.");
                break;
        }
    }


}
