package chapter06;

import java.util.Scanner;

public class Exercise621 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un numero entre 1 y 99999: ");
        int numero = sc.nextInt();
        System.out.print("Los digitos del numero son: ");
        mostrarDigitos(numero);

    }

    private static void mostrarDigitos(int numero) {
        int e = residuo(numero);
        int d = residuo(parteEntera(numero,10));
        int c = residuo(parteEntera(numero,100));
        int b = residuo(parteEntera(numero,1000));
        int a = parteEntera(numero,10000);
        System.out.printf("%d %d %d %d %d\n",a,b,c,d,e);
    }

    private static int parteEntera(int a, int b) {
        return a / b;
    }

    private static int residuo(int a) {
        return a % 10;
    }
}
