// Print square with entered character
package chapter06;

import java.util.Scanner;

public class Exercise619 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter the side of the square: ");
        int side = sc.nextInt();

        System.out.print("Enter character for the square: ");
        char character = sc.next().charAt(0);

        for (int i = 1; i <= side; i++) {
            System.out.println();
            for (int j = 1; j <= side; j++) {
                System.out.print(character);
            }
        }
    }
}
