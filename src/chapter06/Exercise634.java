
//System binary, octal and hexadecimal

package chapter06;

import java.util.Scanner;

public class Exercise634 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a number: ");
        int numero = sc.nextInt();
        System.out.printf("The number %d of binary is: ",numero);
        binario(numero);
        System.out.printf("The number %d of octal is: ",numero);
        octal(numero);
        System.out.printf("The number %d of hexadecimal is: ",numero);
        hexadecimal(numero);
    }

    private static void binario(int numero) {
//        String numberbi = "";
        StringBuilder numberbi = new StringBuilder();
        while (cociente(numero,2) >= 2) {
//            numberbi += residuo(numero,2);
            numberbi.append(residuo(numero,2));
            numero = cociente(numero,2);
        }
//        numberbi += residuo(numero,2);
        numberbi.append(residuo(numero,2));
//        numberbi += cociente(numero,2);
        numberbi.append(cociente(numero,2));
        System.out.println(invertirTexto(numberbi.toString()));

    }

    private static void octal(int numero) {
//        String numberocta = "";
        StringBuilder numberocta = new StringBuilder();
        while (cociente(numero,8) >= 8) {
//            numberocta += residuo(numero,8);
            numberocta.append(residuo(numero,8));
            numero = cociente(numero,8);
        }

//        numberocta += residuo(numero,8);
        numberocta.append(residuo(numero,8));
//        numberocta += cociente(numero,8);
        numberocta.append(cociente(numero,8));
        System.out.println(invertirTexto(numberocta.toString()));
    }

    private static void hexadecimal(int numero) {
//        String numberhexa = "";
        StringBuilder numberhexa = new StringBuilder();
        while (cociente(numero,16) >= 16) {
            if (residuo(numero,16) == 10) {
//                numberhexa += "A";
                numberhexa.append("A");
            }else if (residuo(numero,16) == 11) {
//                numberhexa += "B";
                numberhexa.append("B");
            }else if (residuo(numero,16) == 12) {
//                numberhexa += "C";
                numberhexa.append("C");
            }else if (residuo(numero,16) == 13) {
//                numberhexa += "D";
                numberhexa.append("D");
            }else if (residuo(numero,16) == 14) {
//                numberhexa += "E";
                numberhexa.append("E");
            }else if (residuo(numero,16) == 15) {
//                numberhexa += "F";
                numberhexa.append("F");
            } else {
//                numberhexa += residuo(numero,16);
                numberhexa.append(residuo(numero,16));
            }
            numero = cociente(numero,16);
        }

        if (residuo(numero,16) >= 10) {
//            numberhexa += sistemahexa(numero,residuo(numero,16));
            numberhexa.append(sistemahexa(residuo(numero,16)));
        }else {
//            numberhexa += residuo(numero,16);
            numberhexa.append(residuo(numero,16));
        }

        if (cociente(numero,16) >= 10) {
//            numberhexa += sistemahexa(numero,cociente(numero,16));
            numberhexa.append(sistemahexa(cociente(numero,16)));
        }else {
//            numberhexa += cociente(numero,16);
            numberhexa.append(cociente(numero,16));
        }

        System.out.println(invertirTexto(numberhexa.toString()));
    }

    private static String sistemahexa(int cocienteOResiduo) {
        if (cocienteOResiduo == 10) {
            return  "A";
        }else if (cocienteOResiduo == 11) {
            return  "B";
        }else if (cocienteOResiduo == 12) {
            return  "C";
        }else if (cocienteOResiduo == 13) {
            return  "D";
        }else if (cocienteOResiduo == 14) {
            return  "E";
        }else if (cocienteOResiduo == 15) {
            return  "F";
        }else {
            return "";
        }
    }


    private static int cociente(int numero, int divisor) {
        return numero / divisor;
    }

    private static int residuo(int numero, int divisor) {
        return numero % divisor;
    }

    private static String invertirTexto(String number) {
//        String numeroinvertido = "";
        StringBuilder numeroinvertido = new StringBuilder();
        for (int i = number.length() - 1; i >= 0; i--) {
//            numeroinvertido += number.charAt(i);
            numeroinvertido.append(number.charAt(i));
        }
        return numeroinvertido.toString();
    }
}
