package chapter06;

import java.util.Scanner;

public class Exercise625 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a number less than 10000: ");
        int numero = sc.nextInt();
        System.out.printf("The number less than %d are:\n",numero);
        for (int i = 2; i < numero ; i++) {
            if (esPrimo(i)) {
                System.out.printf("%d ", i);
            }
        }
        System.out.println();

    }

    private static boolean esPrimo(int numero) {
        int contador = 0;
        for (int i = 1; i <= numero; i++) {
            if (numero % i == 0) {
                contador++;
                if (contador > 2) {
                    break;
                }
            }
        }
//        if (contador == 2) {
//            return true;
//        } else {
//            return false;
//        }

        return contador == 2;
    }
}
