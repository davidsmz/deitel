package chapter04;

import java.awt.Graphics;
import javax.swing.JPanel;

public class Exercise41b extends JPanel {

    public void paintComponent(Graphics g) {

        super.paintComponent(g);

        int width = getWidth();
        int height = getHeight();

        int separatex = width / 15;
        int separatey = height / 15;

        int w = 0;
        int h = 0;

        for (int i = 0; i < 14; i++) {
            w += separatex;
            h += separatey;

            g.drawLine(0,height, w,h);
            g.drawLine(width,0,w,h);
        }

        w = 0;
        h = height;

        for (int i = 0; i < 14; i++) {
            w += separatex;
            h -= separatey;

            g.drawLine(0,0,w,h);
            g.drawLine(width,height,w,h);
        }

    }

}
