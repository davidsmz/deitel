package chapter04;

import java.util.Scanner;

public class Exercise437a {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Exercise437a obj = new Exercise437a();
        System.out.print("Escriba un numero positivo: ");
        int n = sc.nextInt();
        double f = obj.factorial(n);


        System.out.printf("El factorial de %d es %f \n",n,f);

        System.out.print("Indique numero de terminos para constante: ");
        int terminos = sc.nextInt();
        double constE = obj.constanteE(terminos);
        System.out.printf("La constante vale aproximadamente: %f\n",constE);
    }

    private double factorial(int n) {
        double fact = 1;

        while (n > 0) {
            fact *= n;
            n--;
        }

        return fact;
    }

    private double constanteE(int terminos) {
        double suma = 1;

        while (terminos > 0) {
            suma += 1 / factorial(terminos);
            terminos--;
        }

        return suma;
    }
}
