package chapter04;

import java.awt.Graphics;
import javax.swing.JPanel;

public class Excercise42a extends JPanel {
    public void paintComponent(Graphics g) {

        super.paintComponent(g);

        int width = getWidth();
        int height = getHeight();

        int separatex = width / 15;
        int separatey = height / 15;

        int w = -separatex;
        int h = 0;

        for (int i = 0; i < 14; i++) {
            w += separatex;
            h += separatey;

            g.drawLine(0,w,h,height);
        }
    }
}
