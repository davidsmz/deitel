package chapter04;

import javax.swing.JFrame;

public class PruebaPanelDibujo {
    public static void main(String[] args) {

        // create the panel with our paint
        PanelDibujo panel = new PanelDibujo();

        // create a new marco for within the panel with our panel
        JFrame aplication = new JFrame();

        // set the marco for out when close
        aplication.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // add the panel on marco
        aplication.add(panel);

        // set size the marco
        aplication.setSize(350,350);

        // do visible the marco
        aplication.setVisible(true);
    }
}
