package chapter04;

import java.awt.Graphics;
import javax.swing.JPanel;

public class Exercice41a extends JPanel {

    public void paintComponent(Graphics g) {

        super.paintComponent(g);

        int width = getWidth();
        int height = getHeight();

        int separetex = width / 15;
        int separatey = height / 15;

        width = 0;

        for (int i = 1; i <= 14; i++) {
            height -= separatey;
            width += separetex;
            g.drawLine(0,0,width,height);
        }
    }
}
