package chapter04;

import java.awt.Graphics;
import javax.swing.JPanel;

public class PanelDibujo extends JPanel {

    // Paint the x in the panel
    public void paintComponent(Graphics g) {
        // call paintComponet for show the panel correctly
        super.paintComponent(g);

        int width = getWidth();   // width total
        int height= getHeight();  // height total

        // paint line from top left to bottom right
        g.drawLine(0,0,width,height);

        // paint line from top right to bottom left
        g.drawLine(width,0,0,height);
    }
}
