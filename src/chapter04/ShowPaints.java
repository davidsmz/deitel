package chapter04;

import javax.swing.JFrame;

public class ShowPaints {
    public static void main(String[] args) {
        // Exercise 4.1.a del chapter 4
        // Exercice41a paint = new Exercice41a();

        // Exercise 4.1.b del chapter 4
        // Exercise41b paint = new Exercise41b();

        // Exercise 4.2.a del chapter 4
        // Excercise42a paint = new Excercise42a();

        // Exercise 4.2.b del chapter 4
        Exercise42b paint = new Exercise42b();
        JFrame picture = new JFrame();

        picture.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        picture.add(paint);
        picture.setSize(600,600);
        picture.setVisible(true);
    }
}
