package chapter04;

import java.awt.Graphics;
import javax.swing.JPanel;

public class Exercise42b extends JPanel {
    public void paintComponent(Graphics g) {

        super.paintComponent(g);

        int width = getWidth();
        int height = getHeight();

        int separatex = width / 15;
        int separatey = height / 15;

        int w = 0;
        int h = 0;

        for (int i = 0; i < 14; i++) {
            w += separatex;
            h += separatey;

            g.drawLine(0, w, h, height);
            g.drawLine(0, w, height - h, 0);
        }

        w = 0;
        h = 0;
        for (int i = 0; i < 14; i++) {
            w += separatex;
            h += separatey;

            g.drawLine(width, h, width - w, height);
            g.drawLine(width, h, w, 0);

        }
    }
}